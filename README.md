# TP Programmation Robotique L3 
version etudiants

**Pour rédiger vos Jupyter notebook vous pouvez utiliser le service Colaboratory de Google accessible [ici](https://colab.research.google.com)**

*Fichier -> Importer*


```shell
python -m venv reachy-env
source reachy-env/bin/activate  # Sur Windows, utilisez `reachy-env\Scripts\activate`
```

```shell
pip install -r requirements.txt
```